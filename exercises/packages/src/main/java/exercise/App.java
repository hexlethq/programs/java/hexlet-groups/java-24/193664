// BEGIN
package exercise;

import exercise.geometry.Segment;

public class App {

    public static double[] getMidpointOfSegment(double[][] segment) {
        double[] midPoint = new double[]{(segment[0][0] + segment[1][0]) / 2, (segment[0][1] + segment[1][1]) / 2};
        return midPoint;
    }

    public static double[][] reverse(double[][] segment) {
        double[] begin = Segment.getBeginPoint(segment);
        double[] end = Segment.getEndPoint(segment);

        double[][] reveredSegment = new double[][]{end.clone(), begin.clone()};
        return reveredSegment;
    }
}

// END
