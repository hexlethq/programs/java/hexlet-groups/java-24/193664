package exercise;

// BEGIN
public class Kennel {

    private static String[][] puppiesStorage = new String[100][2];

    public static int getPuppyCount() {
        int count = 0;
        for (int i = 0; i < puppiesStorage.length; i++) {
            if (puppiesStorage[i][0] != null) {
                count++;
            }
        }
        return count;
    }


    public static void addPuppy(String[] puppy) {
        if (getPuppyCount() < 100) {
            puppiesStorage[getPuppyCount()] = puppy;
        }
    }

    public static void addSomePuppies(String[][] puppies) {
        for (int i = 0; i < puppies.length; i++) {
            addPuppy(puppies[i]);
        }
    }

    public static boolean isContainPuppy(String name) {
        for (int i = 0; i < puppiesStorage.length; i++) {
            if (puppiesStorage[i][0] != null && puppiesStorage[i][0].equals(name)) {
                return true;
            }
        }

        return false;

    }

    public static String[][] getAllPuppies() {
        String[][] allPuppies = new String[getPuppyCount()][2];
        for (int i = 0; i < allPuppies.length; i++) {
            allPuppies[i][0] = puppiesStorage[i][0];
            allPuppies[i][1] = puppiesStorage[i][1];
        }
        return allPuppies;
    }

    public static String[] getNamesByBreed(String breed) {
        int countOfDodByBreed = 0;
        for (int i = 0; i < puppiesStorage.length; i++) {
            if (puppiesStorage[i][1] != null && puppiesStorage[i][1].equals(breed)) {
                countOfDodByBreed++;
            }
        }
        String namesByBreed[] = new String[countOfDodByBreed];
        int y = 0;
        for (int i = 0; i < puppiesStorage.length; i++) {
            if (puppiesStorage[i][1] != null && puppiesStorage[i][1].equals(breed)) {
                namesByBreed[y] = puppiesStorage[i][0];
                y++;
            }
        }
        return namesByBreed;
    }

    public static void resetKennel() {
        puppiesStorage = new String[100][2];
    }

    public static boolean removePuppy(String name) {
        boolean compact = false;
        if (isContainPuppy(name)) {
            for (int i = 0; i < puppiesStorage.length; i++) {
                if (puppiesStorage[i][0] != null && puppiesStorage[i][0].equals(name)) {
                    compact = true;
                }
                if (compact && i != puppiesStorage.length - 1) {
                    puppiesStorage[i] = puppiesStorage[i + 1];
                } else if (compact && i == puppiesStorage.length - 1) {
                    puppiesStorage[i][0] = null;
                    puppiesStorage[i][1] = null;
                }
            }

        }
        return compact;
    }

}

// END
