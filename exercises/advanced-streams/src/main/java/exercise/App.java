package exercise;

import java.util.Arrays;
import java.util.stream.Collectors;

// BEGIN
public class App {
    public static String getForwardedVariables(String config) {
        return Arrays.stream(config.split("\n"))
                .filter(item -> item.startsWith("environment"))
                .flatMap(item -> Arrays.stream(item.split("\"")))
                .flatMap(item -> Arrays.stream(item.split(",")))
                .filter(item -> item.contains("X_FORWARDED_"))
                .map(item -> item.replace("X_FORWARDED_", ""))
                .collect(Collectors.joining(","));
    }
}
//END
