package exercise;

// BEGIN
public class NegativeRadiusException extends Exception {
    private String errorMsg;

    public NegativeRadiusException(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
// END
