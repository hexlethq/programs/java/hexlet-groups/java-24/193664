package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
public class InMemoryKV implements KeyValueStorage {
    private Map<String, String> keyValueMap = new HashMap<>();

    public InMemoryKV(Map<String, String> initialMap) {
//        keyValueMap = initialMap;
        this.keyValueMap.putAll(initialMap);
    }

    @Override
    public void set(String key, String value) {
        this.keyValueMap.put(key, value);
    }

    @Override
    public void unset(String key) {
        keyValueMap.remove(key);
    }

    @Override
    public String get(String key, String defaultValue) {
        String resultedString = keyValueMap.get(key);
        return resultedString == null ? defaultValue : resultedString;
    }

    @Override
    public Map<String, String> toMap() {
//        return keyValueMap;
        return new HashMap<>(this.keyValueMap);
    }
}
// END
