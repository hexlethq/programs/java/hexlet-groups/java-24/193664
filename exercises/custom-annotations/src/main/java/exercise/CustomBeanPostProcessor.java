package exercise;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

// BEGIN
@Component
public class CustomBeanPostProcessor implements BeanPostProcessor {
    private Map<String, String> params = new HashMap<>();
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomBeanPostProcessor.class);

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean.getClass().isAnnotationPresent(Inspect.class)) {
            String infoLevel = bean.getClass().getAnnotation(Inspect.class).level();
            params.put(beanName, infoLevel);
        }
        return BeanPostProcessor.super.postProcessBeforeInitialization(bean, beanName);
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (beanName.equals(entry.getKey())) {
                return Proxy.newProxyInstance(
                        bean.getClass().getClassLoader(),
                        bean.getClass().getInterfaces(),
                        ((proxy, method, args) -> {
                            String message = String.format(
                                    "Was called method: %s() with arguments: %s",
                                    method.getName(),
                                    Arrays.toString(args)
                            );
                            if (entry.getValue().equals("info")) {
                                LOGGER.info(message);
                            } else if (entry.getValue().equals("debug")){
                                LOGGER.debug(message);
                            }
                            return method.invoke(bean, args);
                        })
                );
            }
        }
        return BeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
    }

}

// END
