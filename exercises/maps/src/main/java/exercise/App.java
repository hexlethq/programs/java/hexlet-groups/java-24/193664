package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
public class App {
    public static HashMap getWordCount(String sentence) {
        String[] arr = sentence.split(" ");
        HashMap<String, Integer> wordsHashMap = new HashMap<>();
        if (sentence.isEmpty()) {
            return wordsHashMap;
        }
        for (String s : arr) {
            Integer count;
            if (wordsHashMap.containsKey(s)) {
                count = wordsHashMap.get(s) + 1;
            } else {
                count = 1;
            }
            wordsHashMap.put(s, count);
        }
        return wordsHashMap;

    }

    public static String toString(HashMap<String, Integer> wordsCount) {
        if (wordsCount.isEmpty()) {
            return "{}";
        }
        String result = "{" + "\n";
        for (Map.Entry<String, Integer> word : wordsCount.entrySet()) {
            result += (
                    "  " + word.getKey() + ": " + word.getValue() + "\n"
            );
        }
        result += "}";
        return result;
    }
}
//END
