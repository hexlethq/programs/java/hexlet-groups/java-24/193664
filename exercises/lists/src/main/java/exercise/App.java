package exercise;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

// BEGIN
public class App {
    public static boolean scrabble(String symbols, String word) {
        List<Character> list = symbols.toLowerCase().chars().mapToObj(n -> (char) n).collect(Collectors.toList());
        List<Character> list2 = word.toLowerCase().chars().mapToObj(n -> (char) n).collect(Collectors.toList());
        for (int i = 0; i < list.size(); i++) {
            if (list2.contains(list.get(i))) {
                list2.remove(list.get(i));
            }
        }

        return list2.size() == 0;

    }
}
//END
