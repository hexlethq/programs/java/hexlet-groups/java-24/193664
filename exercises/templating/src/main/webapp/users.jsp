<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- BEGIN -->
 <!DOCTYPE html>
 <html>
     <head>
         <meta charset="UTF-8">
         <title>User</title>
         <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
             rel="stylesheet"
             integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We"
             crossorigin="anonymous">
     </head>
     <body class="bg-info container text-center">
     <table class="table">
              <thead>
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Firstname</th>
                  <th>Lastname</th>
                </tr>
              </thead>
              <tbody>
              <c:forEach var="user" items="${users}">
                             <tr>
                               <td>${user.get("id")}</td>
                               <td><a href='/users/show?id=${user.get("id")}'>${user.get("firstName")}</td>
                               <td>${user.get("lastName")}</td>
                             </tr>
              </c:forEach>
                           </tbody>
                         </table>
                       </div>

     </body>
 </html>
<!-- END -->
