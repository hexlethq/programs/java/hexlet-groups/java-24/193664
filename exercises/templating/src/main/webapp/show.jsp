<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!-- BEGIN -->
 <html>
     <head>
         <meta charset="UTF-8">
         <title>Show user</title>
         <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
             rel="stylesheet"
             integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We"
             crossorigin="anonymous">
     </head>
     <body class="bg-info text-center">
     <div class="container">
     <table class="table">
         <thead>
           <tr>
             <th>ID</th>
             <th>Firstname</th>
             <th>Lastname</th>
             <th>Email</th>
           </tr>
         </thead>
          <tbody>
               <tr>
                 <td>${user.get("id")}</td>
                 <td>${user.get("firstName")}</td>
                 <td>${user.get("lastName")}</td>
                 <td>${user.get("email")}</td>
               </tr>
             </tbody>
           </table>
         </div>
        <a href="/users/delete?id=${user.get("id")}" class="btn btn-danger btn-sm">Delete user</a>
     </body>
 </html>
<!-- END -->
