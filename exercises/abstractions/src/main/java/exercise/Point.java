package exercise;

class Point {

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    private int x;
    private int y;


    private Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    // BEGIN
    static Point makePoint(int x, int y) {
        return new Point(x, y);
    }

    static int getX(Point point) {
        return point.getX();
    }

    static int getY(Point point) {
        return point.getY();
    }

    static String pointToString(Point point) {
        return "(" + point.getX() + ", " + point.getY() + ")";
    }

    static int getQuadrant(Point point) {
        int result = 0;
        if (point.getX() == 0 || point.getY() == 0) {
            return 0;
        }
        if (point.getX() > 0 && point.getY() > 0) {
            return 1;
        }
        if (point.getX() < 0 && point.getY() > 0) {
            return 2;
        }
        if (point.getX() < 0 && point.getY() < 0) {
            return 3;
        }
        return 4;
    }

    public static Point getSymmetricalPointByX(Point point) {
        return new Point(point.getX() * -1, point.getY());
    }

    public static double calculateDistance(Point point1, Point point2){
        return Math.sqrt(Math.pow(point2.getX() - point1.getX(),2) + Math.pow(point2.getY() - point1.getY(),2));
    }
    // END
}
