package exercise;

class App {
    // BEGIN
    public static int[] reverse(int[] numbers) {
        int temp;
        for (int i = 0; i < numbers.length / 2; i++) {
            temp = numbers[numbers.length - 1 - i];
            numbers[numbers.length - 1 - i] = numbers[i];
            numbers[i] = temp;
        }
        return numbers;
        // END
    }

    public static int getIndexOfMaxNegative(int[] numbers) {
        int index = -1;
        int maxNegative = Integer.MIN_VALUE;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] < 0 && maxNegative < numbers[i]) {
                maxNegative = numbers[i];
                index = i;
            }
        }
        return index;
    }
}
