package exercise;

import java.util.Map;
import java.util.logging.Logger;


class App {
    private static final Logger LOGGER = Logger.getLogger("AppLogger");

    // BEGIN
    public static Map<String, Integer> getMinMax(int[] numbers) {
        MaxThread maxThread = new MaxThread(numbers);
        MinThread minThread = new MinThread(numbers);

        maxThread.start();
        LOGGER.info(maxThread.getName() + "has been started");
        minThread.start();
        LOGGER.info(minThread.getName() + "has been started");
        try {
            maxThread.join();
            LOGGER.info(maxThread.getName() + "has been finished");
            minThread.join();
            LOGGER.info(minThread.getName() + "has been finished");
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
        return Map.of("max", maxThread.getMax(), "min", minThread.getMin());
    }
    // END
}

