package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int firstSide, int secondSide, int thirdSide) {
        String result = null;
        if (firstSide + secondSide > thirdSide && secondSide + thirdSide > firstSide && firstSide
                + thirdSide > secondSide) {
            if (firstSide == secondSide && secondSide == thirdSide) {
                result = "Равносторонний";
            } else if (firstSide != secondSide && secondSide != thirdSide && firstSide != thirdSide) {
                result = "Разносторонний";
            } else if (firstSide == secondSide || secondSide == thirdSide || firstSide == thirdSide) {
                result = "Равнобедренный";
            }
        } else {
            result = "Треугольник не существует";
        }
        return result;
    }

    // END
}
