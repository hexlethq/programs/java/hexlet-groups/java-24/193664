package exercise;

// BEGIN
public class ReversedSequence implements CharSequence {

    private String str;

    public ReversedSequence(String str) {
        this.str = reverse(str);
    }

    @Override
    public int length() {
        return str.length();
    }

    @Override
    public char charAt(int index) {
        return str.charAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return str.subSequence(start, end);
    }

    @Override
    public String toString() {
        return str;
    }

    private String reverse(String str) {
        StringBuilder reversedString = new StringBuilder(str).reverse();
        return reversedString.toString();
    }
}
// END
