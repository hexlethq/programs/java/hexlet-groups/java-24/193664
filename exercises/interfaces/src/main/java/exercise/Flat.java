package exercise;

// BEGIN
public class Flat implements Home{
    double area;
    double balconyArea;
    int floor;
    @Override
    public double getArea() {
        return area + balconyArea;
    }

    @Override
    public String toString(){
        String str = ("Квартира площадью "+ getArea() + " метров на " + floor + " этаже");
        return str;
    }

    @Override
    public int compareTo(Home another) {
    if (this.getArea() > another.getArea()){
        return 1;
    } else if(this.area == another.getArea()){
        return 0;
    } else {
        return -1;
    }
    }

    public Flat(double area, double balconyArea, int floor) {
        this.area = area;
        this.balconyArea = balconyArea;
        this.floor = floor;
    }
}
// END
