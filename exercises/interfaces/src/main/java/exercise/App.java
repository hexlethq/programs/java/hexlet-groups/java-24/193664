package exercise;

import java.util.List;
import java.util.stream.Collectors;

// BEGIN
public class App {

    public static List<String> buildAppartmentsList(List<Home> apartList, int number) {
        return apartList.stream()
                .sorted()
                .limit(number)
                .map(Home::toString)
                .collect(Collectors.toList());
    }
}
// END
