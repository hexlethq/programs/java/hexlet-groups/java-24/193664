package exercise;

import java.util.*;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static String[][] enlargeArrayImage(String[][] image){
        String[][] stringsStream = Arrays.stream(image)//преобразуем String[][] в стрим массивов
                //дублируем каждый массив 1 раз
                .flatMap(arr -> Stream.of(arr, arr))
                //дублируем элемент каждого массива 1 раз
                .map(arr -> Arrays.stream(arr).flatMap(el -> Stream.of(el, el)))
                //преобразуем результат в массив стрингов
                .map(arr -> arr.toArray(String[]::new))
                //преобразуем результат в массив массивов стрингов
                .toArray(String[][]::new);

        return stringsStream;
    }
}
// END
