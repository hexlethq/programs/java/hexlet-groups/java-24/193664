package exercise;

class App {
    public static void numbers() {
        // BEGIN
        System.out.println((8 / 2) + (100 % 3));
        // END
    }

    public static void strings() {
        String strLanguage = "Java ";
        // BEGIN
        String strWorks = "works ";
        String strOn = "on ";
        String strJVM = "JVM";
        String res = strLanguage + strWorks + strOn + strJVM;
        System.out.println(res);
        // END
    }

    public static void converting() {
        Number soldiersCount = 300;
        String name = " spartans";
        // BEGIN
        String res = soldiersCount + name;
        System.out.println(res);
        // END
    }
}
