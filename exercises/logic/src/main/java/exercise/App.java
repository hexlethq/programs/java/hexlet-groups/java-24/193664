package exercise;

class App {
    public static boolean isBigOdd(int number) {
        // BEGIN
        if (number % 2 == 1 && number >= 1001) {
            return true;
        }
        // END
        return false;
    }

    public static void sayEvenOrNot(int number) {
        // BEGIN
        if (number % 2 == 0) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
        // END
    }

    public static void printPartOfHour(int minutes) {
        // BEGIN
        if (minutes >= 0 && minutes <= 14) {
            System.out.println("First");
            return;
        }
        if (minutes >= 15 && minutes <= 30) {
            System.out.println("Second");
            return;
        }
        if (minutes >= 31 && minutes <= 45) {
            System.out.println("Third");
            return;
        }
        if (minutes >= 46 && minutes <= 60) {
            System.out.println("Fourth");
            return;
        }
        // END
    }
}
