package exercise;

import java.lang.reflect.Field;
import java.util.*;

// BEGIN
public class Validator {

    public static List validate(Address address) {
        List<String> notValidFields = new ArrayList<>();
        for (Field field : address.getClass().getDeclaredFields()) {
            if (field.getAnnotation(NotNull.class) != null) {
                field.setAccessible(true);
                try {
                    if (field.get(address) == null) {
                        notValidFields.add(field.getName());
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

        }
        return notValidFields;
    }

    public static Map<String, List<String>> advancedValidate(Address address) {
        Map<String, List<String>> notValidFields = new HashMap<>();
        for (Field field : address.getClass().getDeclaredFields()) {
            if (field.getAnnotation(NotNull.class) != null) {
                field.setAccessible(true);
                try {
                    Object fieldValue = field.get(address);
                    if (fieldValue == null) {
                        notValidFields.put(field.getName(), Collections.singletonList("can not be null"));
                    } else if (field.getAnnotation(MinLength.class) != null) {
                        if (fieldValue.toString().length() < field.getAnnotation(MinLength.class).value()) {
                            notValidFields.put(field.getName(), Collections.singletonList("length less than " + field.getAnnotation(MinLength.class).value()));
                        }
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

        }
        return notValidFields;
    }
}
// END
