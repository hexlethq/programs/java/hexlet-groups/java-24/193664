package exercise.controller;

import exercise.ResourceNotFoundException;
import exercise.model.Comment;
import exercise.model.Post;
import exercise.repository.CommentRepository;
import exercise.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/posts")
public class CommentController {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private PostRepository postRepository;

    // BEGIN
    @GetMapping("/{postId}/comments")
    public Iterable<Comment> getPostComments(@PathVariable(name = "postId") long id) {
        return commentRepository.findCommentsByPostId(id);
    }

    @GetMapping("/{postId}/comments/{commentId}")
    public Comment getCommentByCommentAndPostId(
            @PathVariable(name = "postId") long postId,
            @PathVariable(name = "commentId") long commentId
    ) {
        return commentRepository.findCommentByIdAndPostId(commentId, postId)
                .orElseThrow(() -> new ResourceNotFoundException("Comment not found"));
    }

    @PostMapping("/{postId}/comments")
    public String createComment(
            @PathVariable(name = "postId") long postId,
            @RequestBody Comment comment
    ) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new ResourceNotFoundException("Post not found"));

        comment.setPost(post);
        commentRepository.save(comment);
        return "OK";
    }

    @PatchMapping("/{postId}/comments/{commentId}")
    public void editPostComment(
            @PathVariable("postId") long postId,
            @PathVariable("commentId") long commentId,
            @RequestBody Comment comment
    ) {
        Comment dbComment = commentRepository.findCommentByIdAndPostId(commentId, postId)
                .orElseThrow(() -> new ResourceNotFoundException("Comment not found"));

        dbComment.setContent(comment.getContent());
        commentRepository.save(comment);
    }

    @DeleteMapping("/{postId}/comments/{commentId}")
    public void deletePostComment(
            @PathVariable("postId") long postId,
            @PathVariable("commentId") long commentId
    ) {
        Comment comment = commentRepository.findCommentByIdAndPostId(commentId, postId)
                .orElseThrow(() -> new ResourceNotFoundException("Comment not found"));

        commentRepository.delete(comment);
    }
    // END
}
