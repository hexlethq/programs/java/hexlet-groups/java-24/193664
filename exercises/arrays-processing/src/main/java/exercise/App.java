package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int[] getElementsLessAverage(int[] arr) {
        if (arr.length == 0) {
            return arr;
        }
        double sumOfElements = 0;
        for (int i = 0; i < arr.length; i++) {
            sumOfElements += arr[i];
        }
        double average = sumOfElements / arr.length;
        int[] result = new int[arr.length];
        int k = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] <= average) {
                result[k] = arr[i];
                k++;
            }
        }
        return Arrays.copyOf(result, k);
    }

    public static int getSumBeforeMinAndMax(int[] arr) {
        if (arr.length == 0) {
            return 0;
        }
        int resultedSum = 0;
        int max = arr[0];
        int min = arr[0];
        int indexMax = -1;
        int indexMin = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
                indexMax = i;
            }
            if (arr[i] < min) {
                min = arr[i];
                indexMin = i;
            }
        }
        int from = indexMin < indexMax ? indexMin : indexMax;
        int to = indexMin > indexMax ? indexMin : indexMax;
        arr = Arrays.copyOfRange(arr, from + 1, to);
        for (int i = 0; i < arr.length; i++) {
            resultedSum += arr[i];
        }
        return resultedSum;
    }
    // END
}
