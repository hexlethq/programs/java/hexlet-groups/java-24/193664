package exercise.connections;

import exercise.TcpConnection;

// BEGIN
public class Disconnected implements Connection{
    TcpConnection tcpConnection;
    public Disconnected(TcpConnection tcp){
        this.tcpConnection = tcp;
    }
    @Override
    public void connect() {
        this.tcpConnection.setCurrentState(new Connected(tcpConnection));
        System.out.println("connected");
    }

    @Override
    public void disconnect() {
        System.out.println("Error: connection disconnected.");
    }

    @Override
    public void write(String data) {
        System.out.println("Error: connection disconnected");
    }

    @Override
    public String getCurrentState() {
        return "disconnected";
    }
}
// END
