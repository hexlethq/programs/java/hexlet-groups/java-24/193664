package exercise;

import exercise.connections.Connection;
import exercise.connections.Disconnected;

// BEGIN
public class TcpConnection {

    private Connection connectionState;
    private String ip;
    private int port;

    public TcpConnection(String ip, int port) {
        this.connectionState = new Disconnected(this);
        this.ip = ip;
        this.port = port;
    }

    public String getCurrentState() {
        return this.connectionState.getCurrentState();
    }

    public void connect() {
        this.connectionState.connect();
    }

    public void disconnect() {
        this.connectionState.disconnect();
    }

    public void write(String data) {
        this.connectionState.write(data);
    }

    public void setCurrentState(Connection currentState) {
        this.connectionState = currentState;
    }
}
// END
