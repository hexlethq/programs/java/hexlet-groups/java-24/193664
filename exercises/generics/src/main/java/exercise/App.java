package exercise;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

// BEGIN
public class App {

    public static ArrayList<HashMap> findWhere(ArrayList<HashMap> books, HashMap<String, String> where) {
        if (where == null) {
            return new ArrayList<>();
        }
        ArrayList<HashMap> resultedList = new ArrayList<>();
        for (HashMap<String, String> book : books) {
            Set<Entry<String, String>> bookAttributes = book.entrySet();
            if (bookAttributes.containsAll(where.entrySet())) {
                resultedList.add(book);
            }
        }
        return resultedList;
    }
}
//END
