package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

class App {
    // BEGIN
    public static String buildList(String[] strs) {
        if (strs.length == 0) {
            return "";
        } else {
            StringBuilder sb = new StringBuilder("<ul>\n");
            for (int i = 0; i < strs.length; i++) {
                sb.append("  <li>")
                        .append(strs[i])
                        .append("</li>\n");
            }
            sb.append("</ul>");
            return sb.toString();
        }

    }

    public static String getUsersByYear(String[][] arr, int year) {
        StringBuilder sb = new StringBuilder();
        String delimiter = ";";
        for (int i = 0; i < arr.length; i++) {
            LocalDate date = LocalDate.parse(arr[i][1]);
            if (year == date.getYear()) {
                sb.append(arr[i][0]).append(delimiter);
            }
        }
        String[] result = sb.length() > 0 ? sb.toString().trim().split(delimiter) : new String[]{};
        return buildList(result);
    }
    // END

    //     Это дополнительная задача, которая выполняется по желанию.
    public static String getYoungestUser(String[][] users, String date) throws Exception {
        // BEGIN
        String name = "";
        String defaultDate = "01 Jan 1900";
        DateTimeFormatter rule = DateTimeFormatter.ofPattern("dd MMM yyyy", Locale.ENGLISH);
        LocalDate dateToCompareWith = LocalDate.parse(date, rule);
        LocalDate tmp = LocalDate.parse(defaultDate, rule);
        for (int i = 0; i < users.length; i++) {
            LocalDate currentUsersDate = LocalDate.parse(users[i][1]);
            if (currentUsersDate.isBefore(dateToCompareWith) && currentUsersDate.isAfter(tmp)) {
                tmp = currentUsersDate;
                name = users[i][0];
            }
        }
        if (tmp.toString().equals(defaultDate)) {
            return "";
        } else {
            return name;
        }
        // END
    }
}
