package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String sentence) {
        String[] words = sentence.split(" ");
        String result = "";
        for (String word : words) {
            result = result + word.substring(0, 1).toUpperCase();
        }
        return result;
    }
    // END
}
