package exercise;

import java.util.List;
import java.util.Arrays;

// BEGIN
public class App {
    public static int getCountOfFreeEmails(List<String> emailsList){
        return (int) emailsList.stream()
                .map(email -> email.split("@")[1])
                .filter(domen -> domen.equals("gmail.com")
                        || domen.equals("yandex.ru")
                        || domen.equals("hotmail.com")
                ).count();

    }
}


// END
