package exercise;

import java.util.ArrayList;

class SafetyList {
    // BEGIN
    private ArrayList<Integer> list = new ArrayList<>();

    public synchronized void add(int number) {
        this.list.add(number);
    }

    public int get(int index) {
        return this.list.get(index);
    }

    public int getSize() {
        return this.list.size();
    }
    // END
}
