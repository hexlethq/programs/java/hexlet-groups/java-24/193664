package exercise;

import java.util.Random;

// BEGIN
public class ListThread extends Thread{
    private SafetyList safetyList;
    private Random random = new Random();

    public ListThread(SafetyList safetyList) {
        this.safetyList = safetyList;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            synchronized(this) {
                try {
                    Thread.sleep(1);
                    this.safetyList.add(this.random.nextInt());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

// END
