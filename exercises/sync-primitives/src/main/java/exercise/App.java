package exercise;

class App {

    public static void main(String[] args) throws InterruptedException {
        // BEGIN
        SafetyList list = new SafetyList();

        Thread firstThread = new Thread(new ListThread(list));
        Thread secondThread = new Thread(new ListThread(list));

        firstThread.start();
        secondThread.start();

        firstThread.join();
        secondThread.join();

        System.out.println(list.getSize());
        // END
    }
}

