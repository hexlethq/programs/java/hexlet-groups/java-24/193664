package exercise.servlet;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.ArrayUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

public class UsersServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }

        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");

        showUser(request, response, id);
    }

    private List getUsers() throws JsonProcessingException, IOException {
        // BEGIN
        final String usersData = Files.readString(Paths.get(this.getClass().getClassLoader().getResource("users.json").getFile()));
        final ObjectMapper om = new ObjectMapper();
        final List<Map<String, String>> users = om.readValue(usersData, new TypeReference<List<Map<String, String>>>() {
        });
        return users;
        // END
    }

    private void showUsers(HttpServletRequest request,
                           HttpServletResponse response)
            throws IOException {

        // BEGIN
        response.setContentType("text/html;charset=UTF-8");
        List<Map<String, String>> users = getUsers();
        PrintWriter out = response.getWriter();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<table>");
        for (Map<String, String> usersList : users) {
            String id = usersList.get("id");
            String fullName = usersList.get("firstName") + " " + usersList.get("lastName");
            stringBuilder.append("<tr><td>")
                    .append(id)
                    .append("</td>")
                    .append("<td><a href=\"/users/")
                    .append(id)
                    .append("\">")
                    .append(fullName)
                    .append("</a></td></tr>");
        }
        stringBuilder.append("</table>");
        out.print(stringBuilder);
        // END
    }

    private void showUser(HttpServletRequest request,
                          HttpServletResponse response,
                          String id)
            throws IOException {

        // BEGIN
        StringBuilder sb = new StringBuilder();
        List<Map<String, String>> users = getUsers();
        if (users.size() == 0) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "Not found");
            return;
        }
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).get("id").equals(id)) {
                sb.append("<table>");
                String firstName = users.get(i).get("firstName");
                String lastName = users.get(i).get("lastName");
                String email = users.get(i).get("email");

                sb.append("<tr><td>")
                        .append(id)
                        .append("</td><td>")
                        .append(firstName)
                        .append("</td><td>")
                        .append(lastName)
                        .append("</td><td>")
                        .append(email)
                        .append("</td>")
                        .append("</table>");
                PrintWriter pw = response.getWriter();
                pw.println(sb);
                return;
            }
        }
        response.sendError(HttpServletResponse.SC_NOT_FOUND, "Not found");
        return;
        // END
    }
}
