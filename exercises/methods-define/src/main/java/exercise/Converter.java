package exercise;


class Converter {
    // BEGIN
    public static int convert(int arg, String unit) {
        int result;
        if (unit == "Kb") {
            result = arg / 1024;
        } else if (unit == "b") {
            result = arg * 1024;
        } else {
            result = 0;
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println("10 Kb = " + convert(10, "b") + " b");
    }
    // END
}
