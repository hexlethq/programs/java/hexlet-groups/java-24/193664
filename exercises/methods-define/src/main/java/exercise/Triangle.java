package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(double firstSide, double secondSide, double corner) {
        double result;
        double cornerInRadian = ((corner * Math.PI) / 180);
        result = 0.5 * (firstSide * secondSide) * Math.sin(cornerInRadian);
        return result;
    }

    public static void main(String[] args) {
        System.out.println(getSquare(4, 5, 45));
    }
    // END
}
