package exercise;

import java.util.LinkedHashMap;
import java.util.Map;

// BEGIN
public class App {
    public static LinkedHashMap genDiff(Map<String, Object> map1, Map<String, Object> map2) {
        LinkedHashMap<String, String> resultedMap = new LinkedHashMap<>();
        map1.forEach((k, v) -> {
            if (map2.containsKey(k)) {
                if (v.equals(map2.get(k))) {
                    resultedMap.put(k, "unchanged");
                } else {
                    resultedMap.put(k, "changed");
                }
            } else {
                resultedMap.put(k, "deleted");
            }
        });
        map2.forEach((k, v) -> {
            if (!map1.containsKey(k)) {
                resultedMap.put(k, "added");
            }
        });

        return resultedMap;
    }
}
//END
