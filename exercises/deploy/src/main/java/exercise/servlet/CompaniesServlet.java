package exercise.servlet;

import exercise.Data;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Collectors;

public class CompaniesServlet extends HttpServlet {
    private String printCompanies(List<String> companies) {
        return companies.stream().collect(Collectors.joining("\n"));
    }

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {

        // BEGIN

        String searchStr = request.getParameter("search");
        List<String> companies = Data.getCompanies();
        PrintWriter out = response.getWriter();
        if (searchStr == null || searchStr.isEmpty()) {
            out.print(this.printCompanies(companies));
            return;
        } else {
            String filteredCompanies = companies.stream().filter(c -> c.contains(searchStr)).collect(Collectors.joining("\n"));
            if (filteredCompanies.isEmpty()) {
                out.print("Companies not found");
                return;
            }
            out.print(filteredCompanies);

        }
        // END
    }
}
